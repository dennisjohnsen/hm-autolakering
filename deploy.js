require('dotenv').config(); // To be able to use environment variables in JS

console.log('User: ' + process.env.FTP_USER);
console.log('Pass: hidden');
console.log('Host: ' + process.env.FTP_HOST);

var init = require('ftp-deploy');
var ftpDeploy = new init();

var config = {
    user: process.env.FTP_USER,
    password: process.env.FTP_PASSWORD,
    host: process.env.FTP_HOST,
    port: 21,
    localRoot: __dirname + '/public',
    remoteRoot: '/public_html/',
    include: ['*', '**/*', '.htaccess'],
    exclude: [],
    deleteRemote: true,
    forcePasv: true
}

ftpDeploy.deploy(config, function(error, result) {
    if (error) {
        console.log(error);
    }
    else {
        console.log('Finshed: ', result);
    }
});
