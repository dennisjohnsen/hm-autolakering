## HM Autolakering

- Built with [Hugo](https://gohugo.io/)
- Content management through [Forestry](https://forestry.io/)
- Artifact build and deployed through Gitlab Pipelines

### Setup
##### Add environment variables in repository:
  - FTP_USER
  - FTP_PASSWORD
  - FTP_HOST
