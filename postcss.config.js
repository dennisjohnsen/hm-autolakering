module.exports = {
    plugins: {
        autoprefixer: {
            browsers: [
                "> 1%",
                "last 1 versions",
                "safari >= 8",
                "IE 11"
            ]
        },
        cssnano: {
            preset: 'default'
        }
    },
}
