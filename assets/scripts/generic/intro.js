/*  =========================================================================
    INTRO
    ========================================================================= */

(function intro() {
    var introWrapper = $('.js--intro');

    var intro = {
        'list':  introWrapper.find('ul'),
        'item':  introWrapper.find('li'),
        'delay': introWrapper.data('intro-delay'),
        'speed': introWrapper.data('intro-speed'),
    }

    function fader(item) {
        function setSize() {
            intro.list.css({
                width: item.width(),
                height: item.height()
            });
        }

        setSize();

        var galleryResizeThrottle;

        win.on('resize', function() {
            clearTimeout(galleryResizeThrottle);
            galleryResizeThrottle = setTimeout(setSize, 50);
        });

        item.fadeIn().delay(intro.delay).fadeOut(intro.speed, function() {
            if (item.next().length > 0) {
                fader(item.next());
            }
            else {
                fader(item.siblings(':first'));
            }
        });
    }

    intro.item.hide();
    fader(intro.item.first());
})();
