/*  =========================================================================
    GLOBAL
    ========================================================================= */

/*  DOM selectors
*/
var doc =  $(document),
    win =  $(window),
    html = $('html'),
    body = $('body'),
    htmlbody = $('html, body');

var header = {
    'header': $('.c-site-header'),
    'nav':    $('.c-site-header__nav')
}


/*  Global States
*/
var navIsShown =         false,
    navIsRevealed =      false,
    galleryIsMaximized = false;
