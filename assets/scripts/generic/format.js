/*  =========================================================================
    IMAGE FORMAT
    ========================================================================= */

(function imageFormat() {
    $('.o-format').each(function() {
        var wrapper = $(this);
        var img = wrapper.find('.o-format__obj');
        var data = {
            'width':  img.data('width'),
            'height': img.data('height')
        }

        if (data.width && data.height) {
            var config = {
                'ratio': 9 / 16,
                'actualRatio': data.height / data.width
            }

            if (config.actualRatio >= config.ratio) {
                wrapper.addClass('o-format--tall');
            }
            else {
                wrapper.addClass('o-format--wide');
            }
        }
        else {
            wrapper.css('opacity', '0');
            console.log('.o-format__obj is missing data-width and/or data-height attributes set');
        }
    });
})();
