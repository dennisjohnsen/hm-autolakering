/*  =========================================================================
    PARALLAX BACKGROUND
    ========================================================================= */

(function parallaxBg() {
    $('.js--parallaxBg').each(function(){
        var container = $(this);
        var setPos = function() {
            var containerPos = ((win.scrollTop() - (container.offset().top) / 3) - 16);

            // Put together our final background position
            var coords = containerPos + 'px';

            // Move the background
            container.css({
                'transform': 'translate3d(0, ' + coords + ', 0)'
            });
        }

        win.scroll(function() {
            setPos();
        });

        setPos();

        setTimeout(function() {
            setPos();
        }, 50);
    });
})();
