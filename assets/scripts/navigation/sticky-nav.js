/*  =========================================================================
    STICKY NAVIGATION
    ========================================================================= */

var revealNav = {};

(function stickyNav() {
    var previousScroll,
        revealKeep,
        reveilLimiter;

    var cfg = {
        'classSticky':  'is--sticky',
        'classReveal':  'is--revealed',
        'becomeSticky': $('.c-section').first().outerHeight() / 1.2,
    }

    revealNav = {
        'yes': function() {
            navIsRevealed = true;
            header.nav.addClass(cfg.classReveal);
        },
        'no':  function() {
            navIsRevealed = false;
            header.nav.removeClass(cfg.classReveal);
        }
    }

    function stickyNav() {
        var scrollTop = win.scrollTop();

        if (scrollTop > cfg.becomeSticky) {
            navIsShown = true;

            header.nav.addClass(cfg.classSticky);

            if (scrollTop < previousScroll && !galleryIsMaximized) {
                revealNav.yes();
                revealKeep = win.scrollTop() + 100;
            }
            else {
                if (scrollTop > revealKeep) {
                    revealNav.no();
                }
            }

            previousScroll = scrollTop;
        }
        else {
            revealNav.no();

            if (navIsShown) {
                navIsShown = false;

                setTimeout(function() {
                    header.nav.removeClass(cfg.classSticky);
                }, 200);
            }
        }
    }

    win.on('scroll', function() {
        stickyNav();
    });
})();
