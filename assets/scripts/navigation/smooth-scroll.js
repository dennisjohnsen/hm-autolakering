/*  =========================================================================
    SMOOTH SCROLL NAVIGATION
    ========================================================================= */

function smoothScroll(target) {
    htmlbody.animate({
        'scrollTop': $(target).offset().top + 36
    }, 250, function() {
        setTimeout(function() {
            revealNav.no();
        }, 50);
    });
};

(function smoothScrollClick() {
    $('[data-smooth-scroll]').on('click', function(e) {
        var target = $(this).attr('href');
        e.preventDefault();
        
        smoothScroll(target);
    });
})();
