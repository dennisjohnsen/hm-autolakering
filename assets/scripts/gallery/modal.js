/*  =========================================================================
    GALLERY MODAL
    ========================================================================= */

(function galleryModal() {
    // Setting Variables
    var gallery = $('.js--gallery');
    var galleryOpenClass = 'is--maximized';
    var galleryClosingClass = 'is--closing';
    var sel =   {};
    var data =  {};
    var modal = {};

    // Adding backdrop to body
    var backdrop = $('<div class="c-gallery-backdrop"></div>');
    backdrop.appendTo('#galleri');

    // Adding modal markup to each item
    var galleryModal = $('<div class="c-gallery__modal  c-gallery-modal"><div class="c-gallery-modal__img"></div></div>');
    galleryModal.appendTo(gallery);

    // Function for holding variables to be used in different scopes
    function galleryVars(that) {
        sel = {
            'link':     that,
            'thumb':    that.find('.c-gallery__img'),
            'modal':    that.find('.c-gallery-modal'),
            'modalImg': that.find('.c-gallery-modal__img'),
        }
        data = {
            'fullUrl':    sel.link.attr('href'),
            'fullWidth':  sel.thumb.data('width'),
            'fullHeight': sel.thumb.data('height'),
            'thumbUrl':   sel.thumb.attr('src'),
            'thumbPosX':  sel.link.offset().left,
            'thumbPosY':  sel.link.offset().top,
            'viewportW':  win.width(),
            'viewportH':  win.height(),
        }
        modal = {
            'imgRatio': data.fullHeight / data.fullWidth,
            'posX':     -(data.thumbPosX),
            'posY':     -(data.thumbPosY - doc.scrollTop()),
        }
    }

    // Positioning of the modal window
    function positionModal(that) {
        that.css({
            'left':   modal.posX,
            'top':    modal.posY,
            'width':  data.viewportW,
            'height': data.viewportH,
        });
    }

    // Open and close object
    var modalSetState = {
        'open': function() {
            galleryIsMaximized = true;

            // Hide nav if it is revealed
            revealNav.no();

            backdrop.addClass(galleryOpenClass);
            sel.link.addClass(galleryOpenClass);

            // Expand modal to viewport
            positionModal(sel.modal);

            // Setting ratio and thumb url for lazy loading
            sel.modalImg.css({
                'background-image': 'url(' + data.thumbUrl + ')',
                'padding-top':  (data.fullHeight / data.fullWidth * 100) + '%'
            });

            // Load full image by creating a hidden images
            var lazyLoad = $('<img hidden>');
            lazyLoad.attr('src', data.fullUrl);
            lazyLoad.appendTo(sel.modal);

            // Swap image url to full when it's loaded
            lazyLoad.on('load', function() {
                sel.modalImg.css({
                    'background-image': 'url(' + data.fullUrl + ')',
                });

                // Removing <img> from DOM
                this.remove();
            });

            // Setting right image scaling behavior depending on image ratio
            sel.modalImg.css({
                'max-width': data.fullWidth
            });
        },
        'close': function() {
            galleryIsMaximized = false;

            backdrop.removeClass(galleryOpenClass);
            sel.link.addClass(galleryClosingClass);
            sel.link.removeClass(galleryOpenClass);

            setTimeout(function() {
                sel.link.removeClass(galleryClosingClass);
            }, 400);

            sel.modal.css({
                'top':    '',
                'left':   '',
                'width':  '',
                'height': ''
            });
        }
    }

    gallery.on('click', function(e) {
        e.preventDefault();

        galleryVars($(this));

        // Opening modal
        if (!sel.link.hasClass(galleryOpenClass)) {
            modalSetState.open();
        }

        // Closing modal
        else {
            modalSetState.close();
        }
    });

    // Click event on backdrop to close modal
    backdrop.on('click', function(e) {
        e.preventDefault();

        modalSetState.close();
    });

    // Resizing modal
    var galleryResizeThrottle;
    var winWidth = win.width();

    function galleryResize(){
        var that = $('.js--gallery.' + galleryOpenClass);

        // Only do this on X resize, not Y
        if (winWidth != win.width()) {
            // Checking if we have a modal open
            if (that.length == 1) {
                galleryVars(that);

                // Repositioning modal
                positionModal(sel.modal);

                // Update width
                winWidth = win.width();
                delete winWidth;
            }
        }
    }

    win.on('resize', function() {
        clearTimeout(galleryResizeThrottle);
        galleryResizeThrottle = setTimeout(galleryResize, 50);
    });
})();
