/*  =========================================================================
    CONTACT VALIDATION
    ========================================================================= */

(function contactValidation() {
    $('.js--contactValidate').validate({
        errorPlacement: function(error, element) {
            // Append error within linked label
            $(element).closest('form').find('label[for="' + element.attr('id') + '"]').append(error);
        },
        errorElement: 'span',
        rules: {
            'navn': {
                required:  true,
                minlength: 2
            },
            'email': {
                required:  true,
                minlength: 5
            },
            'besked': {
                required:  true,
                minlength: 16
            }
        }
    });
})();
