/*  =========================================================================
    CONTACT FORM
    ========================================================================= */

(function contactForm() {
    var form = $('.js--contactForm');
    var formFeedback = $('.js--contactFeedback');
    var submit = $('.js--submitContactForm');

    $(form).on('submit', function(e) {
        e.preventDefault();

        // Check if form is valid first
        if (form.valid()) {
            // Submit the form using AJAX.
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: form.attr('action'),
                data: form.serialize(),
                success: function(response) {
                    if (response.status == 'success') {
                        formFeedback.show().addClass('is--success').text('Tak for din besked! Vi vender hurtigst muligt tilbage til dig.');
                    }
                    else {
                        formFeedback.show().addClass('is--fail').text('Der var noget galt med din forespørgelse! Prøv igen senere, eller kontakt os over telefon. Error: ' + response.message);
                    }
                    form.remove();
                    smoothScroll('#kontakt');
                }
            });
        }
    });
})();

function enableBtn() {
    $('.js--submitContactForm').removeAttr('aria-disabled').removeAttr('disabled');
    $('.js--recaptcha').hide();
}
